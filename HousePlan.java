package Ass6;

import java.util.LinkedList;
import java.util.List;

public class HousePlan {

        public static void main(String[] args) {
            HouseBuilder builder = new SimpleHouseBuilder();
            HouseBuildingCompany houseBuildingCompany = new HouseBuildingCompany();
            House house = houseBuildingCompany.build(builder);
            System.out.println(house);
        }
    }

    interface HouseBuilder {
        void buildBasement();
        void buildWalls();
        void buildRoof();
        void buildGarage();
        void buildDoors();
        void buildWindows();

        House getHouse();

    }

    class HouseBuildingCompany {

        public House build(HouseBuilder builder) {
            builder.buildBasement();
            builder.buildWalls();
            builder.buildRoof();
            builder.buildGarage();
            builder.buildDoors();
            builder.buildWindows();

            return builder.getHouse();
        }

    }

    class SimpleHouseBuilder implements HouseBuilder {
        private House house;

        public SimpleHouseBuilder() {
            this.house = new House();
        }

        public void buildBasement() {
            this.house.setBasement(new Basement());
        }

        public void buildWalls() {
            for (int i = 0; i < 4; i++)
                this.house.addWall(new Wall());
        }

        public void buildRoof() {
            this.house.setRoot(new Roof());
        }
        public void buildGarage(){
            this.house.setGarage(new Garage());
        }
        public void buildDoors(){
            this.house.setDoors(new Doors());
        }
        public void buildWindows(){
            this.house.setWindows(new Windows());
        }

        public House getHouse() {
            return this.house;
        }
    }

    class Basement {
        @Override
        public String toString() {
            return "Basement";
        }
    }

    class Wall {
        @Override
        public String toString() {
            return "Wall";
        }
    }

    class Roof {
        @Override
        public String toString() {
            return "Roof";
        }
    }

    class Garage{
        @Override
        public String toString(){
            return "Garage";
        }
    }
    class Doors{
        @Override
        public String toString(){
            return "doors";
        }
    }
    class Windows{
        @Override
        public String toString(){
            return "windows";
        }
    }
    class House {
        private Basement basement;

        private List<Wall> walls;

        private Roof roof;
        private Garage garage;
        private Doors doors;
        private Windows windows;

        public House() {
            this.walls = new LinkedList<Wall>();
        }

        public void addWall(Wall wall) {
            this.walls.add(wall);
        }

        public void setRoot(Roof roof) {
            this.roof = roof;
        }

        public void setBasement(Basement basement) {
            this.basement = basement;
        }
        public void setGarage( Garage garage){
            this.garage=garage;

        }
        public void setDoors( Doors doors){
            this.doors=doors;

        }
        public void setWindows( Windows windows){
            this.windows=windows;

        }
        @Override
        public String toString() {
            return "House : " +"\n"+"1."+ this.basement + ". " +"\n"+"2."+ this.walls + ". " +"\n"+ "3."+this.roof +"\n"+"4." +this.garage+"."+"\n"+"5."+this.windows+"."+"\n"+"6."+this.doors +".";
        }
    }

