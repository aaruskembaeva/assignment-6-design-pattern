package Ass6;

    public interface Transport {
        public void load();
        public void drive();
    }
    class TruckTransport implements Transport {
        @Override
        public void load() {
            System.out.println("Loaded Truck with freight");
        }

        @Override
        public void drive() {
            System.out.println("Truck has hit the road");
        }

    }
    class SeaTransport implements Transport {

        @Override
        public void load() {
            System.out.println("Loaded ship with freight");
        }

        @Override
        public void drive() {
            System.out.println("Ship has set Sail");
        }

    }
    interface LogisticsFactory {
        public Transport createTransport(String type);
    }
    class CreateLogistics implements LogisticsFactory {

        public Transport createTransport(String type) {
            switch (type) {
                case "SEA":
                    return new SeaTransport();
                default:
                    return new TruckTransport();
            }
        }

    }
    class LogisticsDriver {
        public static void main(String[] args) {
            CreateLogistics cl = new CreateLogistics();
            System.out.println("1.Handling Land Logistics");
            Transport truck = cl.createTransport("LAND");
            truck.load();
            truck.drive();
            System.out.println("2.Handling Sea Logistics");
            Transport ship = cl.createTransport("SEA");
            ship.load();
            ship.drive();
        }
    }

